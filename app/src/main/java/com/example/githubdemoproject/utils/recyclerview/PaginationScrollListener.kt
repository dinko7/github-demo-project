package com.example.githubdemoproject.utils.recyclerview

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import timber.log.Timber


abstract class PaginationScrollListener(private val layoutManager: LinearLayoutManager) :
    RecyclerView.OnScrollListener() {

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
//        Timber.i("Total items: ${layoutManager.itemCount}")
//        Timber.i("Last item position: ${layoutManager.findLastCompletelyVisibleItemPosition()}")
        if (layoutManager.findLastCompletelyVisibleItemPosition() == (layoutManager.itemCount - 1)) {
            loadMoreItems()
        }
//        recyclerView.scrollTo(dx,dy)
    }

    protected abstract fun loadMoreItems()

}