package com.example.githubdemoproject.utils.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.githubdemoproject.R
import com.example.githubdemoproject.networking.models.Repository
import com.example.githubdemoproject.utils.Constants
import com.example.githubdemoproject.utils.helper.formatUpdatedDateTime
import com.example.githubdemoproject.utils.helper.getDateTimeFormatter
import com.example.githubdemoproject.utils.recyclerview.RecyclerViewItemOnClickListener
import kotlinx.android.synthetic.main.item_repo.view.*
import java.text.SimpleDateFormat

class RepositoriesRecyclerViewAdapter(
    private val repositories: ArrayList<Repository>,
    private val onClickListener: RecyclerViewItemOnClickListener<Repository>
) :
    RecyclerView.Adapter<RepositoriesRecyclerViewAdapter.ViewHolder>() {

    companion object {
        private val UpdatedAtLatestFirstComparator: (Repository, Repository) -> Int =
            { repo1, repo2 ->
                val inputFormat = getDateTimeFormatter()
                val date1 = inputFormat.parse(repo1.updated_at)
                val date2 = inputFormat.parse(repo2.updated_at)
                date2.compareTo(date1)
            }
    }

    class ViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val recyclerViewItem = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_repo, parent, false)
        return ViewHolder(recyclerViewItem)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.apply {
            text_repo_name.text = repositories[position].full_name
            text_repo_status.text = formatUpdatedDateTime(repositories[position].updated_at)
            setOnClickListener { onClickListener.onItemClicked(repositories[position]) }
        }
    }

    override fun getItemCount(): Int {
        return repositories.size
    }

    fun updateContent(newTracks: List<Repository>) {
        repositories.clear()
        repositories.addAll(newTracks)
        repositories.sortWith(UpdatedAtLatestFirstComparator)
        notifyDataSetChanged()
    }
}
