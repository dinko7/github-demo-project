package com.example.githubdemoproject.utils.helper

import com.example.githubdemoproject.utils.Constants
import com.example.githubdemoproject.utils.extensions.formatDateTimeString
import java.text.SimpleDateFormat

fun formatUpdatedDateTime(updatedDateTime: String) : String =
    String.format(Constants.UPDATED_DATETIME_FORMAT, updatedDateTime.formatDateTimeString())

fun getDateTimeFormatter(): SimpleDateFormat = SimpleDateFormat(Constants.DATETIME_FORMAT_RECEIVED)