package com.example.githubdemoproject.utils.extensions

import com.example.githubdemoproject.utils.Constants.DATETIME_FORMAT_DISPLAY
import com.example.githubdemoproject.utils.helper.getDateTimeFormatter
import java.text.SimpleDateFormat


fun String.formatDateTimeString() : String {
    val parser =  getDateTimeFormatter()
    val formatter = SimpleDateFormat(DATETIME_FORMAT_DISPLAY)
    return formatter.format(parser.parse(this))
}