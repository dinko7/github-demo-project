package com.example.githubdemoproject.utils.recyclerview

interface RecyclerViewItemOnClickListener<T> {
    fun onItemClicked(item: T)
}