package com.example.githubdemoproject.utils

import java.text.SimpleDateFormat

object Constants {
    const val BASE_URL = "https://api.github.com/"
    const val DATETIME_FORMAT_RECEIVED = "yyyy-MM-dd'T'HH:mm:ss'Z'"
    const val DATETIME_FORMAT_DISPLAY = "dd MMM, yyyy HH:mm"
    const val UPDATED_DATETIME_FORMAT = "Updated on %s"
}