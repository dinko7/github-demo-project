package com.example.githubdemoproject.di

import com.example.githubdemoproject.networking.repository.GithubRepository
import com.example.githubdemoproject.screens.fragment.repositories.RepositoriesViewModel
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {
    fun inject(githubRepository: GithubRepository)
    fun inject(viewModel: RepositoriesViewModel)
}