package com.example.githubdemoproject.di

import com.example.githubdemoproject.networking.api.GithubApi
import com.example.githubdemoproject.networking.api.GithubApiClient
import com.example.githubdemoproject.networking.repository.GithubRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Singleton
    @Provides
    fun provideApi(): GithubApi = GithubApiClient.retrofitService

    @Provides
    fun provideTrendingRepository() = GithubRepository()
}