package com.example.githubdemoproject.screens.fragment.repositoryDetails

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import com.example.githubdemoproject.R
import com.example.githubdemoproject.screens.activity.SharedViewModel
import com.example.githubdemoproject.utils.helper.formatUpdatedDateTime
import kotlinx.android.synthetic.main.fragment_repository_details.*
import kotlinx.android.synthetic.main.item_repo.view.*

class RepositoryDetailsFragment : Fragment() {

    private val sharedViewModel: SharedViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_repository_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initObservers()
    }

    private fun initObservers() {
        sharedViewModel.selectedRepository.observe(viewLifecycleOwner, {
            updateUI()
        })
    }

    private fun updateUI() {
        sharedViewModel.selectedRepository.value?.let {
            text_repo_name.text = it.full_name
            text_repo_description.text = it.description
            text_repo_owner.text = it.owner.login
            text_repo_status.text = formatUpdatedDateTime(it.updated_at)
        }
    }
}