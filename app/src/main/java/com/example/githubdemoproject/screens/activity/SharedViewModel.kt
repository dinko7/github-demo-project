package com.example.githubdemoproject.screens.activity

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.githubdemoproject.base.BaseViewModel
import com.example.githubdemoproject.networking.models.Repository

/**
 * [SharedViewModel] is activity-level viewmodel which is used to pass
 * the selected repository from recycler view to the details fragment.
 */
class SharedViewModel : BaseViewModel() {

    private val _selectedRepository = MutableLiveData<Repository>()
    val selectedRepository: LiveData<Repository>
        get() = _selectedRepository


    fun selectRepository(repository: Repository) {
        _selectedRepository.value = repository
    }
}