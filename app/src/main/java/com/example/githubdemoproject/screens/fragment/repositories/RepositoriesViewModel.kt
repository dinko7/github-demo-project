package com.example.githubdemoproject.screens.fragment.repositories

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.githubdemoproject.base.BaseViewModel
import com.example.githubdemoproject.di.DaggerAppComponent
import com.example.githubdemoproject.networking.api.GithubApiClient
import com.example.githubdemoproject.networking.models.Repository
import com.example.githubdemoproject.networking.repository.GithubRepository
import kotlinx.coroutines.launch
import javax.inject.Inject

class RepositoriesViewModel : BaseViewModel() {

    @Inject
    lateinit var githubRepository : GithubRepository

    private val _repositories = MutableLiveData<List<Repository>>()
    val repositories: LiveData<List<Repository>>
        get() = _repositories

    private var lastQuery: String = ""
    private var currentResultPage: Int = 1


    private fun getRepositories(keyword: String, page: Int? = null, onSuccess: (List<Repository>) -> Unit) {
        startLoading()
        viewModelScope.launch {
            processNetworkRequest(githubRepository.getRepositories(keyword, page)) {
                onSuccess(it)
            }
        }
    }

    /**
     * [getQueryResult] method is used to search repositories by keyword
     */
    fun getQueryResult(keyword: String) {
        getRepositories(keyword) {
            _repositories.value = it

            //reset old query
            lastQuery = keyword
            currentResultPage = 1
        }
    }

    /**
     * [getNextPageQueryResult] is used for getting more result of the previous query.
     */
    fun getNextPageQueryResult() {
        currentResultPage = currentResultPage.inc()
        getRepositories(lastQuery, currentResultPage) {
            _repositories.value = (_repositories.value as ArrayList).apply { addAll(it) }
        }
    }

}