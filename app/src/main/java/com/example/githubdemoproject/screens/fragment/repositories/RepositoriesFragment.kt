package com.example.githubdemoproject.screens.fragment.repositories

import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.githubdemoproject.R
import com.example.githubdemoproject.base.BaseFragment
import com.example.githubdemoproject.networking.models.Repository
import com.example.githubdemoproject.screens.activity.SharedViewModel
import com.example.githubdemoproject.utils.recyclerview.RecyclerViewItemOnClickListener
import com.example.githubdemoproject.utils.adapter.RepositoriesRecyclerViewAdapter
import com.example.githubdemoproject.utils.recyclerview.PaginationScrollListener
import kotlinx.android.synthetic.main.fragment_repositories.*
import timber.log.Timber

class RepositoriesFragment : BaseFragment() {

    private val repositoriesViewModel: RepositoriesViewModel by viewModels()
    private val sharedViewModel: SharedViewModel by activityViewModels()

    private lateinit var recyclerViewAdapter: RepositoriesRecyclerViewAdapter
    private lateinit var recyclerViewLayoutManager: LinearLayoutManager
    private lateinit var recyclerViewScrollListener: PaginationScrollListener

    private val recyclerViewItemOnClickListener =
        object : RecyclerViewItemOnClickListener<Repository> {
            override fun onItemClicked(item: Repository) {
                sharedViewModel.selectRepository(item)
                findNavController().navigate(R.id.action_repositoriesFragment_to_repositoryDetailsFragment)
            }

        }

    private val searchViewListener = object : SearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(query: String): Boolean {
            repositoriesViewModel.getQueryResult(query)
            recycler_view_repositories.smoothScrollToPosition(0) //scroll to top after new query
            return false
        }

        override fun onQueryTextChange(newText: String): Boolean {
            return false
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_repositories, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initUI()
        initObservers()
    }

    private fun initObservers() {
        repositoriesViewModel.errors.observe(viewLifecycleOwner, {
            showError(it)
        })

        repositoriesViewModel.loading.observe(viewLifecycleOwner, {
            if (it)
                showLoading()
            else
                hideLoading()
        })

        repositoriesViewModel.repositories.observe(viewLifecycleOwner, {
            updateRecyclerView(it)
        })

    }

    private fun updateRecyclerView(repositories: List<Repository>) {
        recyclerViewAdapter.updateContent(repositories)
    }

    private fun initUI() {
        setHasOptionsMenu(true) //enable search widget in the action bar
        initRecyclerView()
    }


    private fun initRecyclerView() {
        recyclerViewAdapter =
            RepositoriesRecyclerViewAdapter(arrayListOf(), recyclerViewItemOnClickListener)
        recyclerViewLayoutManager = LinearLayoutManager(requireContext())
        recyclerViewScrollListener = object : PaginationScrollListener(recyclerViewLayoutManager) {
            override fun loadMoreItems() {
                repositoriesViewModel.getNextPageQueryResult()
            }
        }

        recycler_view_repositories.apply {
            adapter = recyclerViewAdapter
            layoutManager = recyclerViewLayoutManager
            addOnScrollListener(recyclerViewScrollListener)
        }
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)

        inflater.inflate(R.menu.menu_search, menu)
        val item = menu.findItem(R.id.action_search)
        val searchView = item.actionView as SearchView
        menu.findItem(R.id.action_search).apply {
            setShowAsAction(MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW or MenuItem.SHOW_AS_ACTION_IF_ROOM)
            actionView = searchView
        }
        searchView.setOnQueryTextListener(searchViewListener)
    }
}