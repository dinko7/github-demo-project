package com.example.githubdemoproject.base

import android.app.Dialog
import androidx.fragment.app.Fragment
import com.example.githubdemoproject.R
import com.example.githubdemoproject.utils.manager.DialogManager
import com.google.android.material.snackbar.Snackbar

/**
 * The [BaseFragment] is [Fragment] with common properties shared by fragments in this app.
 * Such common properties are:
 *      a) Error displaying
 *      b) Loading dialog display
 */
abstract class BaseFragment : Fragment() {

    protected val dialogManager by lazy { DialogManager(requireContext()) }

    private var loadingDialog : Dialog? = null

    open fun showMessage(message: String) {
        Snackbar.make(requireView(), message, Snackbar.LENGTH_SHORT).show();
    }

    open fun showError(message: String?) {
        showMessage(
            if (message.isNullOrBlank())
                getString(R.string.an_unknown_error_occurred)
            else
                getString(R.string.network_error_with_cause, message)
        )
    }

    open fun showLoading() {
        if (loadingDialog == null)
           loadingDialog = dialogManager.getLoadingDialog()
        loadingDialog?.show()
    }

    open fun hideLoading() {
        loadingDialog?.dismiss()
        loadingDialog = null
    }
}