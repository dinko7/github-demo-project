package com.example.githubdemoproject.base

import com.example.githubdemoproject.utils.errorhandling.NetworkResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.HttpException


abstract class BaseRepository {

    protected suspend fun <T> callApi(apiCall: suspend () -> T): NetworkResult<T> {
        return withContext(Dispatchers.IO) {
            try {
                NetworkResult.Success(apiCall())
            } catch (e: Exception) {
                when (e) {
                    is HttpException -> {
                        NetworkResult.Error("Network error ${e.code()}: ${e.message()}")
                    }
                    else -> NetworkResult.Error(e.message.orEmpty())
                }

            }
        }
    }
}