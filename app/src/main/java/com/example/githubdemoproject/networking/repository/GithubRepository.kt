package com.example.githubdemoproject.networking.repository

import com.example.githubdemoproject.base.BaseRepository
import com.example.githubdemoproject.di.DaggerAppComponent
import com.example.githubdemoproject.networking.api.GithubApi
import com.example.githubdemoproject.networking.models.Repository
import com.example.githubdemoproject.utils.errorhandling.NetworkResult
import javax.inject.Inject

class GithubRepository : BaseRepository(),IGithubRepository {

    @Inject
    lateinit var service: GithubApi

    init {
        DaggerAppComponent.create().inject(this)
    }

    override suspend fun getRepositories(keyword: String, page: Int?): NetworkResult<List<Repository>> {
        return callApi { service.getRepositories(keyword, page).items }
    }

}