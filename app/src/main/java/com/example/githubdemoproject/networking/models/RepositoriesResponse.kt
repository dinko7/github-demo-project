package com.example.githubdemoproject.networking.models

data class RepositoriesResponse(
    val incomplete_results: Boolean,
    val items: List<Repository>,
    val total_count: Int
)