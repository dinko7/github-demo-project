package com.example.githubdemoproject.networking.repository

import com.example.githubdemoproject.networking.models.Repository
import com.example.githubdemoproject.utils.errorhandling.NetworkResult

interface IGithubRepository {
    suspend fun getRepositories(keyword: String, page: Int?) : NetworkResult<List<Repository>>
}