package com.example.githubdemoproject.networking.api

import com.example.githubdemoproject.networking.models.RepositoriesResponse
import com.example.githubdemoproject.utils.Constants.BASE_URL
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

private val interceptor = HttpLoggingInterceptor().apply { setLevel(HttpLoggingInterceptor.Level.BASIC) }
private val client = OkHttpClient.Builder().addInterceptor(interceptor).build();

private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

private val retrofit = Retrofit.Builder()
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .client(client)
    .baseUrl(BASE_URL)
    .build()

interface GithubApi {

    @GET("search/repositories")
    suspend fun getRepositories(
        @Query("q") keyword: String,
        @Query("page") page: Int?
    ): RepositoriesResponse

}

object GithubApiClient {
    val retrofitService : GithubApi by lazy {
        retrofit.create(GithubApi::class.java)
    }
}
